var amqp = require('amqplib/callback_api');

var host = 'amqp://localhost';
var queueName = 'chart-generation-requests';

var amqpConnection = null;

/**
 * start amqp
 */
exports.startAmqp = function () {
    "use strict";
    startConnection(function () {
        startWorker();
    });

};

/**
 * establish a connection
 */
function startConnection(callback) {
    "use strict";
    amqp.connect(host + "?heartbeat=60", function(err, connection) {
        if (err) {
            console.error("[AMQP]", err.message);
            return setTimeout(startConnection, 1000);
        }
        connection.on("error", function(err) {
            if (err.message !== "Connection closing") {
                console.error("[AMQP] conn error", err.message);
            }
        });
        connection.on("close", function() {
            console.error("[AMQP] reconnecting");
            return setTimeout(startConnection, 1000);
        });
        console.log("[AMQP] connected");
        amqpConnection = connection;
        callback();
    });
}

/**
 * start the consumer
 */
function startWorker() {
    "use strict";
    amqpConnection.createConfirmChannel(function(err, ch) {
        if (closeOnErr(amqpConnection, err)) return;
        ch.on("error", function(err) {
            console.error("[AMQP] worker channel error", err.message);
        });
        ch.on("close", function() {
            console.log("[AMQP] channel closed");
        });

        ch.prefetch(10);
        ch.assertQueue(queueName, { durable: true }, function(err, _ok) {
            if (closeOnErr(amqpConnection, err)) return;
            ch.consume(queueName,
                function(msg) {
                    console.log("Processing message: ", msg.content.toString());
                    setTimeout(function() {
                        console.log(" [x] Done");
                        ch.ack(msg);
                    }, 2 * 1000);
                }, { noAck: false });
            console.log("Worker is started");
        });
    });
}

function closeOnErr(connection, err) {
    "use strict";
    if (!err) return false;
    console.error("[AMQP] error", err);
    connection.close();
    return true;
}